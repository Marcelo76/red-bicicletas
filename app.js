var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const jwt = require('jsonwebtoken');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var tokenRouter = require('./routes/token');
var bicicletasRouter = require('./routes/bicicletas');
var bicicletasAPIRouter = require('./routes/api/bicicletas');
var usuariosAPIRouter = require('./routes/api/usuarios');
var authAPIRouter = require('./routes/api/auth');

const passport = require('./config/passport');
const session = require('express-session');

const store = new session.MemoryStore;

app.set({'secretKey':'***...red-bici'});

var app = express();
app.use(session({
  cookie: {maxAge: 240 * 60 * 60 * 1000},
  store: store, 
  saveUninitialized: true,
  resave: 'true',
  secret: '***...red-bici'
}));




const Usuario = require('./models/usuario');
const Token = require('./models/token');

//Conexión a MongoDb
var mongoose = require('mongoose');
const { use } = require('./mailer/mailer');
const { SSL_OP_NETSCAPE_CA_DN_BUG } = require('constants');
const authControllerAPI = require('./controllers/api/authControllerAPI');
var mongoDb = 'mongodb://localhost/red_bicicletas';
mongoose.connect(mongoDb, { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true});
mongoose.Promise = global.Promise;
var db = mongoose.connection
db.on('error', console.error.bind(console, 'MongoDB connection error'));
//Conexión a MongoDb

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use('/api/auth', authAPIRouter);
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/usuarios', usersRouter);
app.use('/bicicletas', loggedIn, bicicletasRouter);
app.use('/api/bicicletas', validarUsuario, bicicletasAPIRouter);
app.use('/api/usuarios', usuariosAPIRouter);
app.use('/token', tokenRouter);

app.get('/login', function(req, res ){
  res.render('session/login')
});

app.post('/login', function(req, res, next){
  passport.authenticate('local',function(err, user, info){
    if(err) return next(err);
    if(!user) return res.render('session/login',{info});
    req.login(user, function(err){
      if(err) return next(err);
      return res.redirect('/');
    });
  })(req, res, next);
});

app.get('/logout', function(req, res ){
  req.logOut();
  res.render('/');
});

app.get('/forgotPasswor', function(req, res ){
  
});

app.post('/forgotPassword', function(req, res ){
  
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});



app.get('/forgotPassword', function(req, res){
  res.render('session/forgotPassword');
});

app.post('/forgotPassword', function(req, res){
  Usuario.find({email: req.body.email}, function(err, user){
    if(!user){return res.render('session/forgotPassword', {info: {message: 'No existe el email para el usuario existente'}}); }
    
    use.resetPassword(function(err){
      if(err)return next(err);
      console.log('session/forgotPasswordMessage');
    });

    res.render('session/forgotPasswordMessage');
  });
});

app.get('reset/resetPassword/:token', function(req, res, next){
  Token.findOne({ token: req.params.token}, function(err, token){
    if(!token){return res.status(400).send({type: 'not-verified', msg: 'No existen usuarios asociados al token. Verifique que su token no haya expirado'});}

    Usuario.find(token._userId, function(err, usuario){
      if(!usuario){return res.status(400).send({msg: 'No existen usuarios asociados al token!'});}
      res.render('session/resetPassword', {errors: {}, usuario: usuario});
    });
  })
});

app.post('/resetPassword', function(req, res){
  if(req.body.password != req.body.confirm_password){
    res.render(
      'session/resetPassword',
       {errors: {confirm_password: {message: 'Contraselña y confirmación de contrseña no coinciden.'}} ,
        Usuario: new Usuario({email: req.body.email}, function(err, usuario){
          usuario.password = req.body.password;
          usuario.save(function(err){
            if(err){
              res.render('session/resetPassword', { errors: err.errors, usuario: new Usuario({email: req.body.email})});
            }else{
              res.redirect('/login');
            }
          })
        })
  })
}});

function loggedIn(req, res, next){
  if(req.res){
    next();
  }else{
    console.log('usuario sion loguearse');
    res.redirect('/login');
  }
}

function validarUsuario(req, res, next){
  jwt.verify(req.headers['x-access-token'],  req.app.get('secretKey'), function(err, decoded){
    if(err){
      res.json({status: "error", message: err.message, data: null});
    }else{
      req.body._userId = decoded.id;
      cionsole.log('jwt-verify:' + decoded);
      netx();
    }
  });
}

module.exports = app;
