var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');
var Usuario = require('../../models/usuario');
var Reserva = require('../../models/reserva');

describe('Testing bicicletas', () => {
    beforeEach(function(done){
        mongoose.connect('mongodb://localhost/testdb', { useCreateIndex: true, useUnifiedTopology: true, useNewUrlParser: true });

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function(){
            console.log('Estamos conectados a la base de datos de pruebas');
            done();
        });
    });

    afterEach(function(done){
        Bicicleta.deleteMany({}, function(err, success){
            if(err)console.log(err);
            //mongoose.connection.close(err =>{
            //    if(err)console.log(err)
            //    done();
            //});
            done();
        });
    });


    describe('Cuando un usuario reserva una vici', () => {
        it('debe existir la reserva', (done) => {
            const usuario = new Usuario({nombre: 'Marcelo'});
            usuario.save();
            const bicicleta = new Bicicleta({code:1, color: "verde", modelo: "urbana"});
            bicicleta.save();

            var hoy = new Date();
            var manana = new Date();
            manana.setDate(hoy.getDate() + 1);
            usuario.reservar(bicicleta.id, hoy, manana, function(err, reserva){
                Reserva.find({}).populate('bicicleta').populate('usuario').exec(function(err, reservas){                    
                    expect(reservas.length).toBe(1);
                    expect(reservas[0].diasDeReserva()).toBe(2);
                    expect(reservas[0].bicicleta.code).toBe(1);
                    expect(reservas[0].usuario.nombre).toBe(usuario.nombre);
                    done();
                });
            });
        });
    });
    

});