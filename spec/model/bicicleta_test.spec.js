var mongoose = require('mongoose');
var bicicleta = require('../../models/bicicleta');
//var originalTimeout = 5000;

describe('Testing bicicletas', () => {
    beforeEach(function(done){        
        
        var mongoDb = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDb, {useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true});

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function(){
            console.log('Estamos conectados a la base de datos de pruebas');
            done();
        });
        
    });

    afterEach(function(done){
        bicicleta.deleteMany({}, function(err, success){
            if(err)console.log(err);
            mongoose.connection.close(err =>{
                if(err)console.log(err)
                done();
            });
            done();
        });
    });

    describe('Bicicleta.createIntance', () => {
        it('Crea una instancia de Bicicleta', (done) =>{
            var bici = bicicleta.createInstance(1, 'verde', 'urbana', [-34.5, -54.1]);

            expect(bici.code).toBe(1);
            expect(bici.color).toBe('verde');
            expect(bici.modelo).toBe('urbana');
            expect(bici.ubicacion[0]).toEqual(-34.5);
            expect(bici.ubicacion[1]).toEqual(-54.1);
            done();
        });
    });

    
    describe('Bicicleta.allBicis', () => {
        it('comnienza vacia', (done) => {
            bicicleta.allBicis(function(err, bicis){
                expect(bicis.length).toBe(0);
                done();
            });            
        });
    });


    describe('Bicicleta.add', () => {
        it('Agregar nueva bicicleta', (done)=> {
            var aBici = new bicicleta({code: 1, color: "verde", modelo: "urbana"});
            bicicleta.add(aBici, function(err, newBici){
                if(err)console.log(err);
                bicicleta.allBicis(function(err, bicis){
                    expect(bicis.length).toEqual(1);
                    expect(bicis[0].code).toEqual(aBici.code);
                    done();
                })
            });
        });
    });

    describe('Bicicleta.findByCode',()=>{
        it('Debe devolver la bici con code 1', (done) => {
            bicicleta.allBicis(function(err, bicis){
                expect(bicis.length).toEqual(0);

                var aBici = new bicicleta({code: 1, color: "verde", modelo: "urbana"});
                bicicleta.add(aBici, function(err, newBici){
                    if(err)console.log(err);

                    var aBici2 = new bicicleta({code: 2, color: "roja", modelo: "urbana"});
                    bicicleta.add(aBici2, function(err, newBici){
                        if(err)console.log(err);
                        bicicleta.findByCode(1, function(err, targetBici){
                            expect(targetBici.code).toEqual(1);
                            expect(targetBici.color).toBe("verde");
                            expect(targetBici.modelo).toBe("urbana");
                            done();
                        });
                    });
                });
            })
        });
    });


});



/*
beforeEach(() => { bicicleta.allBicis = []});   //Antes de cada test ejecuta la función lamda que en éste caso resetea la colección de bicicletas

describe('Bicicleta.allBicis', () => {
    it('Bicicleta comienza vacia', () => {
        expect(bicicleta.allBicis.length).toBe(0);
    });
});

describe('Bicicleta.add', () => {
    it('Agregamos un elemento', () => {
        expect(bicicleta.allBicis.length).toBe(0);

        var a = new bicicleta(1, 'rojo', 'urbana',[-35.427314, -71.663537]);
        bicicleta.add(a)

        expect(bicicleta.allBicis[0]).toBe(a);
    });
});

describe('Bicicleta.findById', () => {
    it('Deve devolver la bicicleta con id 1', () => {
        expect(bicicleta.allBicis.length).toBe(0);

        var a = new bicicleta(1, 'rojo', 'urbana',[-35.427314, -71.663537]);
        var b = new bicicleta(2, 'blanca', 'urbana',[-35.420696, -71.649573]);
        bicicleta.add(a);
        bicicleta.add(b);

        var targetBici = bicicleta.findById(1);
        expect(targetBici.id).toBe(1);
        expect(targetBici.color).toBe(a.color);
        expect(targetBici.modelo).toBe(a.modelo);
    });
});

describe("Bicicleta.removeById", () => {
    it("Debe devolver un array con cero elementos.", () => {
        expect(bicicleta.allBicis.length).toBe(0);

        var bici = new bicicleta(1, 'rojo', 'urbana',[-35.427314, -71.663537]);
        bicicleta.add(bici);

        expect(bicicleta.allBicis.length).toBe(1);
        bicicleta.removeById(1)

        expect(bicicleta.allBicis.length).toBe(0);
    });
});
*/