var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var bicicletaSchema = new Schema({
    code: Number,
    color: String,
    modelo: String,
    ubicacion: {
        type: [Number],
        index: {
            type: '2dsphere',
            sparse: true
        }
    }
});


bicicletaSchema.statics.createInstance = function(code, color, modelo, ubicacion){
    return new this({
        code: code,
        color: color,
        modelo: modelo,
        ubicacion: ubicacion
    });
}


bicicletaSchema.methods.toString = function(){
    return 'code: ' + this.code + '|color: ' + this.color;
}

bicicletaSchema.statics.allBicis = function(bc){            
    //return this.find({}).toArray();
    return this.find({}, bc);
}

bicicletaSchema.statics.add = function(aBici, bc){
    this.create(aBici, bc);
}

bicicletaSchema.statics.findByCode = function(aCode, bc){
    this.findOne({code: aCode}, bc);
}

bicicletaSchema.statics.removeByCode = function(aCode, bc){
    this.deleteOne({code: aCode}, bc);
}

module.exports = mongoose.model('Bicicleta', bicicletaSchema);

/*
var Bicicleta = function(id, color, modelo, ubicacion) {
    this.id = id;
    this.color = color;
    this.modelo = modelo;
    this.ubicacion = ubicacion;
}

Bicicleta.prototype.toString = function() {
    return `id:${this.id} | color:${this.color}`;
}

Bicicleta.allBicis = [];
Bicicleta.add = function(bici){
    Bicicleta.allBicis.push(bici);
}

Bicicleta.findById = function(id){
    var bici = Bicicleta.allBicis.find(b => b.id == id);
    if(bici)
        return bici;
    else            
        throw new Error(`No existe una bicicleta con el id ${id}`);
}       

Bicicleta.removeById = function(id){
    Bicicleta.allBicis = Bicicleta.allBicis.filter(b => b.id != id);
}       

//var a = new Bicicleta(1, 'rojo', 'urbana',[-35.427314, -71.663537]);
//var b = new Bicicleta(2, 'blanca', 'urbana',[-35.420696, -71.649573]);

//Bicicleta.add(a)
//Bicicleta.add(b)

module.exports = Bicicleta;

*/