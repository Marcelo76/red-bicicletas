var mongoose = require('mongoose');
var Reserva = require('./reserva');
const model= require('./bicicleta');
const uniqueValidator = require('mongoose-unique-validator');
var Schema = mongoose.Schema;
const crypto = require('crypto');
const bcrypt = require('bcrypt');   //npm install bcrypt
const Token = require('../models/token');
const mailer = require('../mailer/mailer');
const saltRounds = 10;


const validarEmail = function(valor) {
    const reg = (/^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i)
    return reg.test(valor); 
  }

var usuarioSchema = new Schema({
    nombre: {
        type: String,
        trim: true,
        required: [true, 'El nombre es obligatorio']
    },
    email: {
        type: String,
        trim: true,
        required: [true, 'El email es obligatorio'],
        lowercase: true,
        unique: true,
        validate: [validarEmail, 'Ingrese un email válido'],
        match: [(/^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i)]
    },
    password: {
        type: String,
        required: [true, 'El password es obligatorio']
    },
    passwordResetToken: String,
    passwordResetTokenExpires: Date,
    verificado:{
        type: Boolean,
        default: false
    }
});

usuarioSchema.plugin(uniqueValidator, {message: 'El {PATH} ya existe con otro usuario'});

usuarioSchema.pre('save', function(next){
    if(this.isModified('password')){
        this.password = bcrypt.hashSync(this.password, saltRounds);
    }
    next();
});

usuarioSchema.methods.validPassword = function(password){
    return bcrypt.compareSync(password, this.password);
}

usuarioSchema.methods.reservar = function(biciId, desde, hasta, cb){
    var reserva = new Reserva({usuario: this._id, bicicleta: biciId, desde: desde, hasta: hasta});
    console.log(reserva);
    reserva.save(cb);
}

usuarioSchema.methods.enviar_email_bienvenida = function(cb){
    const token = new token({_userId: this.id, token: crypto.randomBytes(16).toString('hex')});
    const email_destination = this.email;
    token.save(function(err){
        if(err){return console.log(err.message)};

        const mailOptions = {
            from: 'no-reply@redbicicletas.com',
            to: email_destination,
            subject: 'verificación de cuenta',
            text: 'Hola\n\n, Para verificar su cuenta por favor haga click en el siguiente enlace:\n\n' + 'http://localhost:3000/' + '\/token/confirmation\/' + token.token + '\n'
        }

        mailer.sendEmail(mailOptions, function(err){
            if(err){return console.log(err.message)};
            
            console.log('La verificación de emila ha sido enviada a ' + email_destination);
        });
    });
}

usuarioSchema.methods.resetPassword = function(cb){
    const token = new Token({_userId: this.id, token: cryto.randomBytes(16).toString('ex')});
    const email_destination = this.email;
    token.save(function(err){
        if(err){return cb(err)}

        const mailOptions = {
            from: 'no_reply@redbicicletas.com',
            to: email_destination,
            subject: 'Reseteo de contraseña',
            text: 'Hola,\n\n ' + 'Para resetear el password de su cuenta haga click en éste Link: \n\n' +
                    'http://localhost:5000' + '\/password\/' + token.token + '\n' 
        };

        mailer.sendMail(mailOptions, function(err){
            if(err){return cb(err)}

            console.log('Se envio un email al resetear la contraseña al email ' + email_destination);
        });
        cb(null);
    });
}

module.exports = mongoose.model('Usuario', usuarioSchema);
