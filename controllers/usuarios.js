var usuario = require('../models/usuario');
var nodemailer = require('../mailer/mailer');

module.exports = {
    list:function(req, res, next){
        usuario.find({}, (err, usuarios) => {
            res.render('usuario/index', {usuarios:usuario});
        });
    },

    update_get: function(req, res, next){
        usuario.findById(req.params.id, function(err, usuario){
            res.render('usuarios/update', {errors: {}, usuario: usuario});
        })
    },

    update: function(req, res, next){
        var update_values = {nombre: req.body.nombre};
        usuario.findByIdAndUpdate(req.params.id, update_values, function(err, usuario){
            if(err){
                console.log(err);
                res.render('usuarios/update', {errors: err.errors, usuario: new usuario()});
            }else{
                res.redirect('usuarios');
                return;
            }
        });
    },

    create_get: function(req, res, next){
        res.render('usuarios/create', {errors: {}, usuario: new usuario()});
    },

    create: function(req, res, next){
        if(req.body.password != res.body.confirm_password){
            res.render('usuarios/create', {errors: {confirm_password: {message: 'La contraseña y la confirmación de contraseña no coinciden.'}}, usuario: new usuario()});
            return;
        }
        usuario.create({nombre: res.body.nombre, email: res.body.email, password: res.body.password});
        if(err){
            res.render('usuarios/create', {errors: err.errors, usuario: new usuario({nombre: req.body.nombre, email: req.body.email})});
        }else{
            ////nuevoUsuario.enviar_email_bienvenida();
            //nodemailer.sendMail();
            res.redirect('/usuarios');
        }
    },

    delete: function(req, res, next){
        usuario.findByIdAndDelete(req.params.id, function(err){
            if(err){
                next(err);
            }else{
                res.redirect('/usuarios');
            }
        });
    }

}