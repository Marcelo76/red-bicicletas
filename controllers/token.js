var usuario = require('../models/usuario');
var Token = require('../models/token');

module.exports = {
    confirmationContent: function(req, res, next){
        Token.findOne({token: req.params.token}, function(err, token){
            if(!token){return res.status(400).send({type: 'not-verified', msg: 'Token no válido'})}
            usuario.findById(token._userId,  function(err, usuario){
                if(!usuario){return res.status(400).send({msg: 'No se ha encontrado el usuario'})}
                if(usuario.verificado){return res.redirect('/usuarios')}
                usuario.verificado = true;
                usuario.save(function(err){
                    if(err){return res.status(400).send({msg: err.message})}
                    res.redirect('/');
                });
            });
        });
    }
}