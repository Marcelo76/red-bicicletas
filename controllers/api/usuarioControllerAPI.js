var Usuario = require('../../models/usuario');

exports.usuarios_list = function(req, res){
    Usuario.find({}, function(err, usuarios){
        res.status(200).json({
            usuarios: usuarios
        });
    });
}

exports.usuarios_create = function(req, res){
    var usuario = new Usuario({nombre: req.body.nombre});

    usuario.save(function(err){
        res.status(200).json(usuario);
    });
}

exports.usuarios_update = function(req, res){
    var user = Usuario.findById(req.params.id);
    if(user){
        user.nombre = req.body.nombre;
        user.email = req.body.email;
        user.password = req.body.password;

        res.status(200).json({
            usuario: user
        });
    }else{
        res.status(404).json({
            res: `El usuario con el id ${req.params.id} no fue encontrado`
        });
    }
}

exports.usuarios_reservar = function(req, res){
    Usuario.findById(req.body.id, function(err, usuario){
        console.log(usuario);
        usuario.reservar(req.body.bici_id, req.body.desde,  req.body.hasta, function(err){
            console.log('reserva!!!');
            res.status(200).send();
        });
    });
}