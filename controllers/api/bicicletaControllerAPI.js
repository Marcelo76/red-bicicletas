var Bicicleta = require('../../models/bicicleta');

<<<<<<< HEAD
exports.bicicleta_list = function(req, res){    
    res.status(200).json({
        bicicletas: Bicicleta.allBicis
=======
exports.bicicleta_list = function(req, res){
    Bicicleta.find({}, function(err, bicicletas) {
        res.status(200).json({
            bicicletas: bicicletas
        })
>>>>>>> aaf623aa701f4d8a69f2769a3c9df041e16f17ff
    });
    //res.status(200).json({
    //    bicicletas: Bicicleta.allBicis
    //});
}

exports.bicicletas_create = function(req, res){
    var ubicacion = [req.body.lat, req.body.lng];
    var bici = new Bicicleta({ code: req.body.code, color: req.body.color, modelo: req.body.modelo, ubicacion: ubicacion });

    bici.save(function(err) {
        res.status(200).json(bici);
    });

    //var bici = new Bicicleta(req.body.id, req.body.color, req.body.modelo, [req.body.lat, req.body.lng]);

    //Bicicleta.add(bici);

    //res.status(200).json({
    //    bicicleta: bici, req: req.body
    //});
}

exports.bicicletas_delete = function(req, res){
    Bicicleta.removeByCode(req.body.code, function(err) {
        res.status(204).send();
    });
    //Bicicleta.allBicis = Bicicleta.allBicis.filter(b => b.id.toString() !== req.params.id);

    //res.status(200).json({
    //    id: req.params.id, bicicletas: Bicicleta.allBicis
    //});
}

exports.bicicletas_update = function(req, res){
    var bici = Bicicleta.findById(req.params.id);
    if(bici){
        bici.color = req.body.color;
        bici.modelo = req.body.modelo;
        bici.ubicacion = [req.body.lat, req.body.lng];

        res.status(200).json({
            bicicleta: bici
        });
    }else{
        res.status(404).json({
            res: `La bicicleta con el id ${req.params.id} no fue encontrada`
        });
    }
}