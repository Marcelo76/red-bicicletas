var express = require('express');
var tokenController = require('../controllers/token');
var router = express.Router();

router.get('/controller/:token', tokenController.confirmationContent);

module.exports = router;