var express = require('express');
var router = express.Router();
var tokenController = require('../../controllers/token');

router.get('/controllers/:token', tokenController.confirmationContent);

module.exports = router;