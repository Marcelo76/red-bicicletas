var express = require('express');
var bicicletaController = require('../../controllers/api/bicicletaControllerAPI');

var router = express.Router();

router.get('/', bicicletaController.bicicleta_list);
router.post('/create', bicicletaController.bicicletas_create);
router.delete('/delete/:id', bicicletaController.bicicletas_delete);
router.put('/update/:id', bicicletaController.bicicletas_update);

module.exports = router;