var express = require('express');
var usuariosController = require('../../controllers/api/usuarioControllerAPI');

var router = express.Router();

router.get('/', usuariosController.usuarios_list);
//router.get('/create', usuariosController.create_get);
router.post('/create', usuariosController.usuarios_create);
//router.get('/:id/update', usuariosController.update_get);
//router.post('/:id/update', usuariosController.update);
router.post('/update/:id', usuariosController.usuarios_update);
router.delete('/delete/:id', usuariosController.usuarios_reservar);

module.exports = router;