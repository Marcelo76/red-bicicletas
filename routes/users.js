var express = require('express');
var usuarioController = require('../controllers/usuarios');
var router = express.Router();

/* GET users listing. */
//router.get('/', function(req, res, next) {
//  res.send('respond with a resource');
//});

router.get('/', usuarioController.list);
router.get('/create', usuarioController.create_get);
router.post('/create', usuarioController.create);
router.post('/:id/delete', usuarioController.delete);
router.get('/:id/update', usuarioController.update_get);
router.post('/:id/update', usuarioController.update);

module.exports = router;
