var mymap = L.map('main_map').setView([-35.426442, -71.665501], 13);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(mymap);

//L.marker([-35.426442, -71.665501]).addTo(mymap)
//    .bindPopup('A pretty CSS3 popup.<br> Easily customizable.')
//    .openPopup();

$.ajax({
    dataType: "json",
    url: 'api/bicicletas',
    success: function(result){
        console.log(result);
        result.bicicletas.forEach(element => {
            L.marker(element.ubicacion, {title: element.id}).addTo(mymap)
        });
    }
})    
    