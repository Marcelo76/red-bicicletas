const passport = require('passport');
const localStrategy = require('passport-local').Strategy;
const usuario = require('../models/usuario');

passport.use(new localStrategy({
    function(email, password, done){
        usuario.findOne({email: email}, function(err, user){
            if(err) return done(err);
            if(!user) return done(null, false, { message: 'Email incorrecto o inexistente'});
            if(!user.validPassword(password)) return done(null, false, { message: 'Contraseña incorrecta'});

            return done(null, user);

        });
    }
}));

passport.serializeUser(function(use, cb){
    cb(null, use.id);
});

passport.deserializeUser(function(id, cb){
    usuario.findById(id, function(err, usuario){
        cb(err, usuario);
    });
});

module.exports = passport;